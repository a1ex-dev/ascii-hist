# ASCII histogram tool

Usage:
```
go run ascii_hist.go -dir=.
```

In this example it is assumed that reading files sequentially is relatively cheap operation compared to expensive calculation (not in this case) on file data. This is the point to fan out these calculations on multiple CPU cores. The tool does not try to parallelize disk IO as it only inflicts performance degradation due to continuous disk seeks.
