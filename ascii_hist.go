package main

import (
    "path"
    "flag"
    "context"
    "log"
    "runtime"
    "io/ioutil"
    "bufio"
    "io"
    "os"
    "fmt"
    "sync"
)

type CharFreq struct {
    char byte
    count int
}

func (s *CharFreq) CharString() string {
    switch code := int(s.char); {
    case code == 32:
        return "(SPACE)"
    case code == 10:
        return "(LF)"
    case code == 9:
        return "(TAB)"
    case code < 33 ||  code > 126:
        return fmt.Sprintf("(x%02X)", s.char)
    default:
        return string([]byte{s.char})
    }
}

func (s *CharFreq) Char() byte {
    return s.char
}

func (s *CharFreq) Count() int {
    return s.count
}

func fanOut(
    ctx context.Context,
    chars <-chan byte,
) []<-chan *CharFreq {
    cpus := runtime.NumCPU()
    streams := make([]<-chan *CharFreq, cpus)
    for i := 0; i < cpus; i++ {
        streams[i] = calcHist(ctx, chars)
    }
    return streams
}

func calcHist(
    ctx context.Context,
    chars <-chan byte,
) <-chan *CharFreq {
    freqs := make(chan *CharFreq)
    hist := make(map[byte] int)
    go func() {
        defer close(freqs)
        for char := range chars {
            hist[char]++
        }
        for char, count := range hist {
            select {
            case freqs <- &CharFreq{char, count}:
            case <-ctx.Done():
                return
            }
        }
    }()
    return freqs
}

func fanIn(
    ctx context.Context,
    freqStreams ...<-chan *CharFreq,
) <-chan *CharFreq {
    var wg sync.WaitGroup
    wg.Add(len(freqStreams))
    allFreqs := make(chan *CharFreq)
    for _, stream := range freqStreams {
        go func(freqs <-chan *CharFreq) {
            defer wg.Done()
            for freq := range freqs {
                select {
                case allFreqs <- freq:
                case <-ctx.Done():
                    return
                }
            }
        }(stream)
    }
    go func() {
        defer close(allFreqs)
        wg.Wait()
    }()
    return allFreqs
}

func flattenFreqs(
    ctx context.Context,
    freqs <-chan *CharFreq,
) <-chan *CharFreq {
    flattenedFreqs := make(chan *CharFreq)
    hist := make(map[byte] int)
    go func() {
        defer close(flattenedFreqs)
        for freq := range freqs {
            hist[freq.Char()] += freq.Count()
        }
        for char, count := range hist {
            select {
            case flattenedFreqs <- &CharFreq{char, count}:
            case <-ctx.Done():
                return
            }
        }
    }()
    return flattenedFreqs
}

func readDir(
    ctx context.Context,
    reportError func(error),
    dirname string,
) <-chan string {
    filenames := make(chan string)
    go func() {
        defer close(filenames)
        if files, err := ioutil.ReadDir(dirname); err == nil {
            for _, file := range files {
                if !file.IsDir() {
                    select {
                    case filenames <- path.Join(dirname, file.Name()):
                    case <-ctx.Done():
                        return
                    }
                }
            }
        } else {
            reportError(err)
        }
    }()
    return filenames
}

func readFile(
    ctx context.Context,
    reportError func(error),
    filenames <-chan string,
) <-chan byte {
    chars := make(chan byte)
    readBytes := func(filename string) error {
        if file, err := os.Open(filename); err == nil {
            reader := bufio.NewReader(file)
            defer file.Close()
            for {
                if byte, err := reader.ReadByte(); err == nil {
                    select {
                    case chars <- byte:
                    case <-ctx.Done():
                        return ctx.Err()
                    }
                } else if err != io.EOF {
                    return err
                } else {
                    return nil
                }
            }
        } else {
            return err
        }
    }
    go func() {
        defer close(chars)
        for filename := range filenames {
            if err := readBytes(filename); err != nil {
                reportError(err)
                return
            }
        }
    }()
    return chars
}

func main() {
    dirname := flag.String("dir", ".", "directory to read files from")
    flag.Parse()
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()
    onError := func(err error) {
        log.Println(err)
        log.Println("terminating")
        cancel()
    }
    freqs := flattenFreqs(ctx, fanIn(ctx, fanOut(ctx, readFile(ctx, onError, readDir(ctx, onError, *dirname)))...))
    for freq := range freqs {
        fmt.Printf("%8s %10d\n", freq.CharString(), freq.Count())
    }
}
